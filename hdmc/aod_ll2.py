###############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Slastnikova Anna <slastnikova02@mail.ru>
# and Rami Al-Naim <alnaim.ri@starline.ru>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import tempfile
from io import StringIO
import numpy as np
import pyproj
import google.protobuf.text_format as text_format

import lanelet2
from lanelet2.core import getId

from .proto.map import map_pb2
from . import argparser as argparser

proj = pyproj.Proj(proj='utm', zone="36", north=True, ellps='WGS84')
ll2_proj = lanelet2.projection.UtmProjector(lanelet2.io.Origin(60.054150, 30.150428))

UNKNOWN, DOTTED_YELLOW, DOTTED_WHITE, \
SOLID_YELLOW, SOLID_WHITE, DOUBLE_YELLOW, CURB = range(7)



class HDMapServiceWrapper:
        
    @classmethod
    def get_gap_map(cls, buf):

        input_file = tempfile.NamedTemporaryFile(dir= 'tmp/', suffix='.osm', mode='w')
        print(buf.getvalue(), file=input_file)
        buf.close()
        if input_file.tell() <= 1:
            return

        output_file = tempfile.NamedTemporaryFile(dir= 'tmp/', suffix='.osm', mode='r')
        
        buildLaneletMap(input_file.name, output_file.name)

        buf = StringIO()
        buf.write(output_file.read())

        input_file.close()
        output_file.close()

        return buf

def get_pb_from_text_file(filename, pb_value):
    """Get a proto from given text file."""
    with open(filename, 'r') as file_in:
        return text_format.Merge(file_in.read(), pb_value)

def get_pb_from_bin_file(filename, pb_value):
    """Get a proto from given binary file."""
    with open(filename, 'rb') as file_in:
        pb_value.ParseFromString(file_in.read())
    return pb_value


#--- Подготовка болванки Lanelet2 карты ----- 

def prepare_way(boundary_pb): 
    points = []
    boundary_type = boundary_pb.boundary_type[0].types[0]

    node_type = None
    if boundary_type == SOLID_WHITE or boundary_type == SOLID_YELLOW:
        node_type = "solid"
    elif boundary_type == DOTTED_WHITE or boundary_type == DOTTED_YELLOW:
        node_type = "dashed"
    elif boundary_type == CURB:
        node_type = "curbstone_low"

    for point_pb in boundary_pb.curve.segment[0].line_segment.point:
        lon, lat = proj(point_pb.x, point_pb.y, inverse = True)
        proj_point = ll2_proj.forward(lanelet2.core.GPSPoint(lat, lon))

        p = lanelet2.core.Point3d(getId(), proj_point.x, proj_point.y, 25)
        if node_type is not None:
            p.attributes["lanelet_node_type"] = node_type
        points.append(p)

    way = lanelet2.core.LineString3d(getId(), points)
    if boundary_type == SOLID_WHITE or boundary_type == SOLID_YELLOW:
        way.attributes["type"] = "line_thin"
        way.attributes["subtype"] = "dashed"

    elif boundary_type == DOTTED_WHITE or boundary_type == DOTTED_YELLOW:
        way.attributes["type"] = "line_thin"
        way.attributes["subtype"] = "solid"

    elif boundary_type == CURB:
        way.attributes["type"] = "curbstone"
        way.attributes["subtype"] = "low"

    return way

def prepare_lanelets(lane_pb, relations, neighbours):
    
    relation = lanelet2.core.Lanelet(getId(),
                   prepare_way(lane_pb.left_boundary),
                   prepare_way(lane_pb.right_boundary))
    
    relation.attributes["apollo_lane_id"] = lane_pb.id.id

    # Центральная кривая
    points = []
    for point_pb in lane_pb.central_curve.segment[0].line_segment.point:
        lon, lat = proj(point_pb.x, point_pb.y, inverse = True)
        proj_point = ll2_proj.forward(lanelet2.core.GPSPoint(lat, lon))

        p = lanelet2.core.Point3d(getId(), proj_point.x, proj_point.y, 25)
        points.append(p)

    # Атрибуты Lanelet2
    relation.attributes["subtype"] = "road"
    try:
        speed = float(lane_pb.speed_limit) * 3.6
        relation.attributes["speed_limit"] = str(speed)
    except:
        relation.attributes["speed_limit"] = str(lane_pb.speed_limit)
        
    relation.attributes["one_way"] = "yes"
    relation.attributes["location"] = "city"

    # Apollo OpenDRIVE Relation
    if lane_pb.predecessor_id:
        for predecessor in lane_pb.predecessor_id:
            relations.append([predecessor.id, lane_pb.id.id])

    if lane_pb.successor_id:
        for successor in lane_pb.successor_id:
            relations.append([lane_pb.id.id, successor.id])
    
    # Apollo OpenDRIVE Neighbour
    if lane_pb.left_neighbor_forward_lane_id:
        ids = set([neighbour.id for neighbour in lane_pb.left_neighbor_forward_lane_id])
        
        if lane_pb.id.id not in neighbours:
            neighbours[lane_pb.id.id] = {}
        
        for id in ids:
            neighbours[lane_pb.id.id][id] = {"left": True, "forward": True}

    
    if lane_pb.right_neighbor_forward_lane_id:
        ids = set([neighbour.id for neighbour in lane_pb.right_neighbor_forward_lane_id])
        
        if lane_pb.id.id not in neighbours:
            neighbours[lane_pb.id.id] = {}
        
        for id in ids:
            neighbours[lane_pb.id.id][id] = {"left": False, "forward": True}
    
    if lane_pb.left_neighbor_reverse_lane_id:
        ids = set([neighbour.id for neighbour in lane_pb.left_neighbor_reverse_lane_id])
        
        if lane_pb.id.id not in neighbours:
            neighbours[lane_pb.id.id] = {}
        
        for id in ids:
            neighbours[lane_pb.id.id][id] = {"left": True, "forward": False}

    
    if lane_pb.right_neighbor_reverse_lane_id:
        ids = set([neighbour.id for neighbour in lane_pb.right_neighbor_reverse_lane_id])
        
        if lane_pb.id.id not in neighbours:
            neighbours[lane_pb.id.id] = {}
        
        for id in ids:
            neighbours[lane_pb.id.id][id] = {"left": False, "forward": False}
    
    return relation, points

# ---- Объединение Node -----

def get_point_index(way, target_point):
    for i, point in enumerate(way):
        if point.id == target_point.id:
            return i
        
    return None

def get_all_children_by_parent_id(relations, id):
    indexes = np.where(relations[:, 0] == id)
    result = relations[indexes, 1]
    result = result.squeeze()
    return set(result)

def get_all_parents_by_child_id(relations, id):
    indexes = np.where(relations[:, 1] == id)
    result = relations[indexes, 0]
    result = result.squeeze()
    return set(result)

def get_unique_predeccessors(relations):
    result = relations[:, 0]
    result = result.squeeze()
    return set(result)

def merge_by_relation(lanes, relations, centerlines):
    predecessors = get_unique_predeccessors(relations)
    

    for pred_id in predecessors:
        predecessor = lanes[pred_id]
        successors = get_all_children_by_parent_id(relations, pred_id)

        point_left = predecessor.leftBound[-1]
        lx = point_left.x
        ly = point_left.y

        point_right = predecessor.rightBound[-1]
        rx = point_right.x
        ry = point_right.y

        point_center = centerlines[pred_id][-1]
        cx = point_center.x
        cy = point_center.y

        counter = len(successors) + 1

        for suc_id in successors:
            lx += lanes[suc_id].leftBound[0].x
            ly += lanes[suc_id].leftBound[0].y

            rx += lanes[suc_id].rightBound[0].x
            ry += lanes[suc_id].rightBound[0].y

            cx += centerlines[suc_id][0].x
            cy += centerlines[suc_id][0].y

            suc_other_preds = get_all_parents_by_child_id(relations, suc_id)
            suc_other_preds.remove(pred_id)
            counter += len(suc_other_preds)

            for other_pred_id in suc_other_preds:
                pred = lanes[other_pred_id]
                lx += pred.leftBound[-1].x
                ly += pred.leftBound[-1].y

                rx += pred.rightBound[-1].x
                ry += pred.rightBound[-1].y

                cx += centerlines[other_pred_id][-1].x
                cy += centerlines[other_pred_id][-1].y

                lanes[other_pred_id].leftBound[-1]  = point_left
                lanes[other_pred_id].rightBound[-1] = point_right
                centerlines[other_pred_id][-1] = point_center

            lanes[suc_id].leftBound[0]  = point_left
            lanes[suc_id].rightBound[0] = point_right
            centerlines[suc_id][0] = point_center
        
        point_left.x = lx / counter 
        point_left.y = ly / counter 

        point_right.x = rx / counter 
        point_right.y = ry / counter

        point_center.x = cx / counter 
        point_center.y = cy / counter


def merge_neighbours(lanelets, id1, id2, type):
    if type['left']:
        if type['forward']:
            points1 = lanelets[id1].leftBound
            points2 = lanelets[id2].rightBound
        else:
            points1 = lanelets[id1].leftBound
            points2 = lanelets[id2].leftBound.invert()
    else:
        if type['forward']:
            points1 = lanelets[id1].rightBound
            points2 = lanelets[id2].leftBound
        else:
            points1 = lanelets[id1].rightBound
            points2 = lanelets[id2].rightBound.invert()

    # Объединяем точки двух границ в один последовательный массив
    full_points = []
    c = points1[round(len(points1) / 2)]

    if lanelet2.geometry.distance(c, points1[0]) >= \
        lanelet2.geometry.distance(c, points2[0]):
        start = points2[0]
    else:
        start = points1[0]
    
    full_points.append(start)
    i, j = 0, 0
    while i < len(points1) and j < len(points2):
        if lanelet2.geometry.distance(start, points1[i]) > \
            lanelet2.geometry.distance(start, points2[j]):
            start = points2[j]
            j += 1
        else:
            start = points1[i]
            i += 1

        full_points.append(start)

    while i < len(points1):
        full_points.append(points1[i])
        i += 1

    while j < len(points2):
        full_points.append(points2[j])
        j += 1
    
    # Объединяем точки чьи координаты отличаются незначительно:
    i = 1
    while i < len(full_points):
        if lanelet2.geometry.distance(full_points[i], full_points[i-1]) < 0.01:
            del full_points[i]
            i-=1
        i+=1

    new_bound = lanelet2.core.LineString3d(getId(), full_points)
    
    if type['left']:
        if type['forward']:
            lanelets[id1].leftBound = new_bound
            lanelets[id2].rightBound = new_bound
        else:
            lanelets[id1].leftBound = new_bound
            lanelets[id2].leftBound = new_bound.invert()
    else:
        if type['forward']:
            lanelets[id1].rightBound = new_bound
            lanelets[id2].leftBound = new_bound
        else:
            lanelets[id1].rightBound = new_bound
            lanelets[id2].rightBound = new_bound.invert()

# ---------------------------

def buildLaneletMap(input_path, output_path):
    map_pb = get_pb_from_text_file(input_path, map_pb2.Map())
    lanelet_map = lanelet2.core.LaneletMap()

    lanelets = {}
    centerlines = {}
    relations = []
    neighbours = {}
    for lane_pb in map_pb.lane:
        lanelet, centerline = prepare_lanelets(lane_pb, relations, neighbours)
        lanelets[lane_pb.id.id] = lanelet
        centerlines[lane_pb.id.id] = centerline

    relations = np.array(relations)

    while len(neighbours) > 0:
        id = list(neighbours.keys())[0]
        for neighbour in set(neighbours[id]):
            merge_neighbours(lanelets, id, neighbour, neighbours[id][neighbour])

            del neighbours[neighbour][id]
        
        del neighbours[id]

    merge_by_relation(lanelets, relations, centerlines)
    for id in centerlines:
        lanelets[id].centerline = lanelet2.core.LineString3d(getId(), centerlines[id])

    # then we create new map instance, cause the lib doesn't have functionality to remove objects
    lanelet_map = lanelet2.core.LaneletMap()
    for id in lanelets:
        lanelet_map.add(lanelets[id])

    lanelet2.io.write(output_path, lanelet_map, ll2_proj)


def get_LL2_from_AOD_with_console():
    args = argparser.parse()

    if args.zone:
        utmz = {}
        try:
            utmz["zone"] = int(args.zone[0:-1])
            utmz["letter"] = str(args.zone[-1]).upper()
        except:
            sys.exit("Incorrect UTM zone")

        utmz["full"] = args.zone

        if utmz["zone"] > 60 or utmz["zone"] < 1:
            sys.exit("Zone number out of range, must be between 1 and 60")

        if utmz["letter"] not in ["N", "S"]:
            sys.exit("Zone letter out of range, must be N or S")

        global proj
        proj = pyproj.Proj(proj='utm', zone=utmz["zone"], north=(
            utmz["letter"] == "N"), south=(utmz["letter"] == "S"), ellps='WGS84')
        
    buildLaneletMap(args.input_file, args.output_file)

# --- Отладочные функции ----

def _get_apollo_ids_by_way(lanelet_map, way):
    result = [lanelet.attributes["apollo_lane_id"] 
                for lanelet in lanelet_map.laneletLayer.findUsages(way)]
    result += [lanelet.attributes["apollo_lane_id"] 
                for lanelet in lanelet_map.laneletLayer.findUsages(way.invert())]
    return result

def _get_apollo_ids_by_point(lanelet_map, point):
    result = []
    for way in lanelet_map.lineStringLayer.findUsages(point):
        result+= _get_apollo_ids_by_way(lanelet_map, way)
    return result


if __name__ == "__main__":
    get_LL2_from_AOD_with_console()