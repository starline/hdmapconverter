
###############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Slastnikova Anna <slastnikova02@mail.ru>
# and Rami Al-Naim <alnaim.ri@starline.ru>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import argparse

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['aod_ll2', 'll2_aod'], help="Mode: aod_ll2 or ll2_aod")
    parser.add_argument('input_file', help="Input filename")
    parser.add_argument('output_file', help="Output filename")
    parser.add_argument('-z', '--zone', action="store",
                        type=str, help="UTM zone, default: 36N")
    return parser.parse_args()