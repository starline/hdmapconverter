###############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Slastnikova Anna <slastnikova02@mail.ru>
# and Rami Al-Naim <alnaim.ri@starline.ru>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


import math
import sys
import pyproj

from shapely.geometry import LineString
import numpy as np

from lxml import etree
from io import StringIO

from .road import *

from .proto.map import map_pb2
from . import argparser as argparser

borderTypes = {"UNKNOWN": 0,
               "DOTTED_YELLOW": 1,
               "DOTTED_WHITE": 2,
               "SOLID_YELLOW": 3,
               "SOLID_WHITE": 4,
               "DOUBLE_YELLOW": 5,
               "CURB": 6}

proj = pyproj.Proj(proj='utm', zone="36", north=True, ellps='WGS84')


def readNodes(e):
    nodes = {}

    # Read all nodes and their coordinates into an array
    for node in e.findall('node'):
        action = node.get("action")
        if action=='delete':
            continue

        n = Node(node.get("id"), float(
            node.get("lat")), float(node.get("lon")))

        for tag in node.findall('tag'):
            if tag.get('k') == "lanelet_node_type":
                n.lanelet_node_type = tag.get('v')

        nodes[node.get("id")] = n
    return nodes


def readWays(e, nodes):
    ways = {}

    # Read all roads/ways into an array
    for way in e.findall('way'):
        way_id = way.get("id")
        action = way.get("action")
        if action=='delete':
            continue

        w = Way(way_id)

        # Read all information about each way
        for tag in way.findall('tag'):
            if tag.get('k') == "type":
                w.type = tag.get('v')
            elif tag.get('k') == "subtype":
                w.subtype = tag.get('v')

        # Get nodes
        way_nodes = way.findall('nd')

        # Connect to the nodes
        for nd in way_nodes:
            node = nodes[nd.get("ref")]
            node.parents.append(w)
            w.nodes.append(node)

        if len(way_nodes) > 0:
            ways[w.id] = w

    return ways


def readRelations(e, ways):
    all_relations = []
    for rel in e.findall('relation'):
        
        action = rel.get("action")
        if action=='delete':
            continue

        r = Relation(rel.get("id"))
        tag_type = None
        tag_subtype = None
        speed_limit = None
        for tag in rel.findall('tag'):
            if tag.get('k') == "type":
                tag_type = tag.get('v')
            elif tag.get('k') == "subtype":
                tag_subtype = tag.get('v')
            elif tag.get('k') == "speed_limit":
                speed_limit = float(tag.get('v'))

        if tag_type != "lanelet":
            continue

        r.speed_limit = speed_limit

        for member in rel.findall('member'):
            if member.get('type') == "way":
                way = ways[member.get('ref')]

                if member.get('role') == "left":
                    r.left = Member(way)
                    
                    for way_rel in way.relations:
                        if (way_rel.left.way == way):
                            r.neighbours.append(
                                Neighbour(way_rel.id, 
                                        left = True,
                                        forward = False))

                            way_rel.neighbours.append(
                                Neighbour(r.id, 
                                        left = True,
                                        forward = False))
                        
                        elif (way_rel.right.way == way):
                            r.neighbours.append(
                                Neighbour(way_rel.id, 
                                        left = True,
                                        forward = True))

                            way_rel.neighbours.append(
                                Neighbour(r.id, 
                                        left = False,
                                        forward = True))
                                
                elif member.get('role') == "right":
                    r.right = Member(way)

                    for way_rel in way.relations:
                        if (way_rel.left.way == way):
                            r.neighbours.append(
                                Neighbour(way_rel.id, 
                                        left = False,
                                        forward = True))
                            
                            way_rel.neighbours.append(
                                Neighbour(r.id, 
                                        left = True,
                                        forward = True))
                        
                        elif (way_rel.right.way == way):
                            r.neighbours.append(
                                Neighbour(way_rel.id, 
                                        left = False,
                                        forward = False))
                            
                            way_rel.neighbours.append(
                                Neighbour(r.id, 
                                        left = False,
                                        forward = False))

                elif member.get('role') == "centerline":
                    r.central = Member(way)

                way.relations.append(r)

        if r.left is not None and r.right is not None:
            all_relations.append(r)

    return all_relations


def readLanelet2(filename: str = None, xml: StringIO = None):
    if filename is not None:
        lanelet2_map = etree.parse(filename).getroot()
    elif xml is not None:
        lanelet2_map = etree.parse(xml).getroot()
    else:
        return

    nodes = readNodes(lanelet2_map)
    ways = readWays(lanelet2_map, nodes)
    relations = readRelations(lanelet2_map, ways)

    return relations


def get_border_type(type_stat):
    if len(type_stat) == 0:
        return borderTypes["SOLID_WHITE"]

    main_type = max(type_stat, key=type_stat.get)
    secondary_type = max(
        [t for t in type_stat if t != main_type], default=None)

    if main_type is None and secondary_type is None:
        return borderTypes["SOLID_WHITE"]

    result_type = None
    if secondary_type is not None:
        if type_stat[main_type] != type_stat[secondary_type]:
            secondary_type = None

        if secondary_type == 'virtual':
            secondary_type = None

    if secondary_type is not None:
        if main_type in ['virtual']:
            main_type = secondary_type
            secondary_type = None

    if secondary_type is not None:
        if 'curbstone' in main_type or 'curbstone' in secondary_type:
            result_type = "CURB"

        elif 'solid' in main_type or 'solid' in secondary_type:
            result_type = "SOLID_WHITE"

        elif 'dashed' in main_type or 'dashed' in secondary_type:
            result_type = "DOTTED_WHITE"

        elif 'virtual' in main_type or 'virtual' in secondary_type:
            result_type = "UNKNOWN"

        elif 'wall' in main_type or 'wall' in secondary_type:
            result_type = "CURB"

        elif 'other' in main_type or 'other' in secondary_type:
            result_type = "UNKNOWN"

        else:
            result_type = "SOLID_WHITE"

    else:
        if 'curbstone' in main_type:
            result_type = "CURB"

        elif 'solid' in main_type:
            result_type = "SOLID_WHITE"

        elif 'dashed' in main_type:
            result_type = "DOTTED_WHITE"

        elif 'virtual' in main_type:
            result_type = "UNKNOWN"

        elif 'wall' in main_type:
            result_type = "CURB"

        elif 'other' in main_type:
            result_type = "UNKNOWN"

        else:
            result_type = "SOLID_WHITE"

    return borderTypes[result_type]


def way_to_boundary(way):
    points = []
    lanelet_node_type = []
    for node in way.nodes:
        lanelet_node_type.append(node.lanelet_node_type)
        type_stat = dict((i, lanelet_node_type.count(i))
                         for i in lanelet_node_type)
        type_stat.pop(None, None)
        apollo_border_type = get_border_type(type_stat)
        
        x, y = proj(node.lng, node.lat)
        points.append([x, y])

    return apollo_border_type, points


def count_dist_array(p1, p2):
    return count_dist(p1[0], p1[1], p2[0], p2[1])


def count_dist_nodes(n1, n2):
    return count_dist(n1.lat, n1.lng, n2.lat, n2.lng)


def count_dist(x1, y1, x2, y2):
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

# c1, c2 - гипотетические первая и последняя точки центральной прямой,
# p - гипотетически первая точка левой границы
# проверяем, что p лежит слева от c1c2
# https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line
def is_left(c1, c2, p):
    return ((c2.lat - c1.lat)*(p.lng - c1.lng) - (c2.lng - c1.lng)*(p.lat - c1.lat)) < 0

def is_right(c1, c2, p):
    return ((c2.lat - c1.lat)*(p.lng - c1.lng) - (c2.lng - c1.lng)*(p.lat - c1.lat)) > 0

def check_if_correct_dir(rel):
    if len(rel.left.nodes) < 2 or len(rel.right.nodes) < 2:
        return 
    
    lp = [rel.left.nodes[0], rel.left.nodes[1]]
    rp = [rel.right.nodes[0], rel.right.nodes[1]]

    cp = (lp[0] + lp[1] + rp[0] + rp[1]) / 4

    if not is_left(rp[0], rp[1], cp):
        rel.right = Member(rel.right.way, is_reversed = True)
    
    if not is_right(lp[0], lp[1], cp):
        rel.left = Member(rel.left.way, is_reversed = True)
    
    # Перепроверяем, что у центральной кривой из источника верное направление
    if (rel.central is not None) and \
        (count_dist_nodes(rel.left.nodes[0], rel.central.nodes[0]) >
            count_dist_nodes(rel.left.nodes[0], rel.central.nodes[-1])):
        rel.central = Member(rel.central.way, is_reversed = True)

# --------------------------------------

def get_rel_by_first_node(n):
    n_ways = []
    for p in n.parents:
        n_ways.append(p)

    n_rel = []
    for w in n_ways:
        for r in w.relations:
            if w == r.left.way and \
                r.left.nodes.index(n) == 0:
                    n_rel.append(r)

            elif w == r.right.way and \
                r.right.nodes.index(n) == 0:
                    n_rel.append(r)
        
    return n_rel

def get_rel_by_last_node(n):
    n_ways = []
    for p in n.parents:
        n_ways.append(p)

    n_rel = []
    for w in n_ways:
        for r in w.relations:
            if w == r.left.way and \
                r.left.nodes.index(n) == len(r.left.nodes) - 1:
                    n_rel.append(r)
            elif w == r.right.way and \
                r.right.nodes.index(n) == len(r.right.nodes) - 1:
                    n_rel.append(r)
    return n_rel

def get_child_id(left, right):
    ln_rel = get_rel_by_first_node(left.nodes[-1])
    rn_rel = get_rel_by_first_node(right.nodes[-1])
    intersected_relations = set(ln_rel) & set(rn_rel)

    return intersected_relations

def get_parent_id(left, right):
    ln_rel = get_rel_by_last_node(left.nodes[0])
    rn_rel = get_rel_by_last_node(right.nodes[0])
    intersected_relations = set(ln_rel) & set(rn_rel)
    
    return intersected_relations

def find_predecessor(relations):
    predecessors = {}
    for rel in relations:  
        predecessors[rel.id] = []
        parents = get_parent_id(rel.left, rel.right)
        for parent in parents:
            if parent.id == rel.id:
                continue
            predecessors[rel.id].append(parent.id)
    return predecessors

def find_successors(relations):
    successors = {}
    for rel in relations:  
        successors[rel.id] = []
        children = get_child_id(rel.left, rel.right)
        for child in children:
            if child.id == rel.id:
                continue
            successors[rel.id].append(child.id)
    return successors


class LanePbBuilder:
    def __init__(self, map_pb, rel):
        self.map_pb = map_pb
        self.rel = rel

        self.lane_pb = map_pb.lane.add()
        self.lane_pb.id.id = rel.id

        self.lane_pb.turn = 1 # NO_TURN
        self.lane_pb.direction = 1 # FORWARD
        self.lane_pb.type = 2 # CITY_DRIVING
        try:
            self.lane_pb.speed_limit = round(float(rel.speed_limit) / 3.6, 2)
        except ValueError:
            self.lane_pb.speed_limit = 5

        self.prepare_left_boundary_stage_1()
        self.prepare_right_boundary_stage_1()
        self.prepare_central_curve()

        width = self.count_mean_width() / 2
        self.prepare_left_boundary_stage_2(width)
        self.prepare_right_boundary_stage_2(width)
    
    def count_mean_width(self):
        left_border = LineString(self.left_points)
        right_border = LineString(self.right_points)

        return left_border.distance(right_border)

    def prepare_central_curve(self):
        self.central_curve_pb = self.lane_pb.central_curve.segment.add()
        if self.rel.central is not None:
            _, self.central_points = way_to_boundary(self.rel.central)
            length = 0
            x, y = self.central_points[0]
            
            for i in range(len(self.central_points)):
                point_pb = self.central_curve_pb.line_segment.point.add()
                point_pb.x = self.central_points[i][0]
                point_pb.y = self.central_points[i][1]
                length += count_dist_array([x, y], self.central_points[i])
        else:
            length = self.central_points = self.count_central_curve()
        
        self.central_curve_pb.start_position.x = self.central_points[0][0]
        self.central_curve_pb.start_position.y = self.central_points[0][1]
        self.central_curve_pb.s = 0
        self.central_curve_pb.length = length
        self.lane_pb.length = length
            
    def count_central_curve(self):       
    
        # Первая точка
        self.central_points = []
        self.central_points.append(
            [(self.right_points[0][0] + self.left_points[0][0]) / 2,
             (self.right_points[0][1] + self.left_points[0][1]) / 2])

        # Промежуточные точки
        if len(self.left_points) >= len(self.right_points):
            longer_points  = self.left_points
            shorter_points = self.right_points
        else:
            longer_points  = self.right_points
            shorter_points = self.left_points
        
        j = 1
        for i in range(1, len(longer_points)):

            if j > len(shorter_points) - 1:
                j = len(shorter_points) - 1
                
            merge_j = count_dist_array(longer_points[i], shorter_points[j]) < \
                count_dist_array(longer_points[i], shorter_points[j - 1])
            
            if merge_j:
                self.central_points.append(
                    [(longer_points[i][0] + shorter_points[j][0]) / 2,
                     (longer_points[i][1] + shorter_points[j][1]) / 2])
                j += 1
            else:
                self.central_points.append(
                    [(longer_points[i][0] + shorter_points[j - 1][0]) / 2,
                     (longer_points[i][1] + shorter_points[j - 1][1]) / 2])

        # Конечная точка, если необходима
        if j < len(shorter_points) or not merge_j:
            self.central_points.append(
                [(self.right_points[-1][0] + self.left_points[-1][0]) / 2,
                 (self.right_points[-1][1] + self.left_points[-1][1]) / 2])
        
        length = 0
        x, y = self.central_points[0]

        for x, y in self.central_points:
            point_pb = self.central_curve_pb.line_segment.point.add()
            point_pb.x = x
            point_pb.y = y
            length += count_dist_array([x, y], self.central_points[i])
        
        return length

    def prepare_left_boundary_stage_1(self):
        left_boundary_type, self.left_points = way_to_boundary(self.rel.left)
        self.left_boundary_pb = self.lane_pb.left_boundary.curve.segment.add()
        
        self.left_boundary_pb.start_position.x = self.left_points[0][0]
        self.left_boundary_pb.start_position.y = self.left_points[0][1]
        self.left_boundary_pb.s = 0

        boundary_type = self.lane_pb.left_boundary.boundary_type.add()
        boundary_type.s = 0
        boundary_type.types.append(left_boundary_type)

    def prepare_right_boundary_stage_1(self):
        right_boundary_type, self.right_points = way_to_boundary(self.rel.right)
        self.right_boundary_pb = self.lane_pb.right_boundary.curve.segment.add()
        
        self.right_boundary_pb.start_position.x = self.right_points[0][0]
        self.right_boundary_pb.start_position.y = self.right_points[0][1]
        self.right_boundary_pb.s = 0

        boundary_type = self.lane_pb.right_boundary.boundary_type.add()
        boundary_type.s = 0
        boundary_type.types.append(right_boundary_type)
    
    def prepare_left_boundary_stage_2(self, width):
    
        left_sample = self.lane_pb.left_sample.add()
        left_sample.s = 0
        left_sample.width = width

        dist = 0
        x, y = self.left_points[0]

        for i in range(len(self.left_points)):
            point_pb   = self.left_boundary_pb.line_segment.point.add()
            point_pb.x = self.left_points[i][0]
            point_pb.y = self.left_points[i][1]

            dist += count_dist_array([x, y], self.left_points[i])

            left_sample = self.lane_pb.left_sample.add()
            left_sample.s = dist
            left_sample.width = width
        
        self.lane_pb.left_boundary.length = dist
        self.left_boundary_pb.length = dist
    
    def prepare_right_boundary_stage_2(self, width):
    
        right_sample = self.lane_pb.right_sample.add()
        right_sample.s = 0
        right_sample.width = width / 2.0

        dist = 0
        x, y = self.right_points[0]

        for i in range(len(self.right_points)):
            point_pb   = self.right_boundary_pb.line_segment.point.add()
            point_pb.x = self.right_points[i][0]
            point_pb.y = self.right_points[i][1]

            dist += count_dist_array([x, y], self.right_points[i])

            right_sample = self.lane_pb.right_sample.add()
            right_sample.s = dist
            right_sample.width = width
        
        self.lane_pb.right_boundary.length = dist
        self.right_boundary_pb.length = dist
    
    def add_road(self):
        road_pb = self.map_pb.road.add()
        id = "road_" + self.rel.id
        road_pb.id.id = id
        road_pb.type = 2
        section_pb = road_pb.section.add()
        section_pb.id.id = "1"
        section_pb.lane_id.add().id = self.rel.id
    
    def add_road_sample(self):
    
        left_road_border = LineString(self.left_points)
        central_road_line = LineString(self.central_points)
        right_road_border = LineString(self.right_points)

        dist_left = central_road_line.distance(left_road_border)
        dist_right = central_road_line.distance(right_road_border)

        for i in np.arange(0, central_road_line.length, 5):
            left_sample = self.lane_pb.left_road_sample.add()
            left_sample.s = i
            left_sample.width = dist_left

            right_sample = self.lane_pb.right_road_sample.add()
            right_sample.s = i
            right_sample.width = dist_right

    def add_neighbours(self):
        for n in self.rel.neighbours:
            if n.left:
                if n.forward:
                    self.lane_pb.left_neighbor_forward_lane_id.add().id = n.id
                    self.lane_pb.left_neighbor_forward_lane_id.add().id = n.id
                else:
                    self.lane_pb.left_neighbor_reverse_lane_id.add().id = n.id
                    self.lane_pb.left_neighbor_reverse_lane_id.add().id = n.id

            else:
                if n.forward:
                    self.lane_pb.right_neighbor_forward_lane_id.add().id = n.id
                    self.lane_pb.right_neighbor_forward_lane_id.add().id = n.id
                else:
                    self.lane_pb.right_neighbor_reverse_lane_id.add().id = n.id
                    self.lane_pb.right_neighbor_reverse_lane_id.add().id = n.id

def buildTxt(relations, filename=None):
    for rel in relations:
        check_if_correct_dir(rel)

    successors = find_successors(relations)
    predecessors = find_predecessor(relations)

    map_pb = map_pb2.Map()
    for rel in relations:
        builder = LanePbBuilder(map_pb, rel)

        for pred_id in predecessors[rel.id]:
            builder.lane_pb.predecessor_id.add().id = pred_id

        for succ_id in successors[rel.id]:
            builder.lane_pb.successor_id.add().id = succ_id

        builder.add_neighbours()
        builder.add_road()
        builder.add_road_sample()

    if filename is not None:
        map_file = open(filename, 'w')
        map_file.write(str(map_pb))
        map_file.close()
    else:
        return StringIO(str(map_pb))


def get_AOD_from_LL2(xml: StringIO = None):
    if xml is None:
        return

    relations = readLanelet2(xml)
    return buildTxt(relations)


def get_AOD_from_LL2_with_console():
    args = argparser.parse()

    if args.zone:
        utmz = {}
        try:
            utmz["zone"] = int(args.zone[0:-1])
            utmz["letter"] = str(args.zone[-1]).upper()
        except:
            sys.exit("Incorrect UTM zone")

        utmz["full"] = args.zone

        if utmz["zone"] > 60 or utmz["zone"] < 1:
            sys.exit("Zone number out of range, must be between 1 and 60")

        if utmz["letter"] not in ["N", "S"]:
            sys.exit("Zone letter out of range, must be N or S")

        global proj
        proj = pyproj.Proj(proj='utm', zone=utmz["zone"], north=(
            utmz["letter"] == "N"), south=(utmz["letter"] == "S"), ellps='WGS84')

    relations = readLanelet2(args.input_file)
    buildTxt(relations, args.output_file)
    print('Apollo OpenDRIVE map was built successfully. It contains {0} lanes.'.format(
        len(relations)))


if __name__ == "__main__":
    get_AOD_from_LL2_with_console()
