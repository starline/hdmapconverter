###############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#                                                                           
# Created by Slastnikova Anna <slastnikova02@mail.ru>   
# and Rami Al-Naim <alnaim.ri@starline.ru>                          
#   
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

class Way:
    
    def __init__(self, id):
        self.id = id
        self.nodes = []
        self.type = None
        self.subtype = None
        self.relations = []

class Member:
    def __init__(self, way, is_reversed = False):
        self.way = way
        self.reversed = is_reversed

        if self.reversed:
            # print(self.way.id, is_reversed)
            self.nodes = list(reversed(self.way.nodes))
        else:
            self.nodes = self.way.nodes

class Node:
    
    def __init__(self, id, lat = None, lng = None):
        self.id = id
        self.lat = lat
        self.lng = lng
        self.lanelet_node_type = None
        self.parents = []
    
    def __sub__(self, value, /):
        return Node(self.id + "-" + value.id, 
                    self.lat - value.lat, 
                    self.lng - value.lng)

    def __add__(self, value):
        return Node(self.id + "+" + value.id, 
                    self.lat + value.lat, 
                    self.lng + value.lng)

    def __truediv__(self, value):
        return Node(self.id + "_divided", 
                    self.lat / value, 
                    self.lng / value)


class Relation:

    def __init__(self, id):
        self.id = id
        # self.members = []
        self.left = None
        self.right = None
        self.central = None
        self.speed_limit = None
        self.neighbours = []
    

class Neighbour:
    # if left == false, then right
    # if forward == false, then reverse
    def __init__(self, id, left=False, forward=True):
        self.id = id
        self.left = left
        self.forward = forward
        
        

