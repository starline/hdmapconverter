###############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Slastnikova Anna <slastnikova02@mail.ru>
# and Rami Al-Naim <alnaim.ri@starline.ru>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


import sys
from . import argparser as argparser
from .ll2_aod import get_AOD_from_LL2_with_console
from .aod_ll2 import get_LL2_from_AOD_with_console

if __name__ == "__main__":

    args = argparser.parse()
    
    if args.mode == "ll2_aod":
        get_AOD_from_LL2_with_console()
    elif args.mode == "aod_ll2":
        get_LL2_from_AOD_with_console()
    else:
        sys.exit("Unknown mode")