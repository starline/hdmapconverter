###############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#                                                                           
# Created by Slastnikova Anna <slastnikova02@mail.ru>                    
#   
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


EXEC_PATH=$PWD
SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPT_PATH
cd ..

protoc -I=./ --python_out=./ \
                             ./hdmc/proto/common/error_code.proto \
                             ./hdmc/proto/common/header.proto \
                             ./hdmc/proto/common/pointcloud.proto \
                             ./hdmc/proto/common/pnc_point.proto \
                             ./hdmc/proto/common/geometry.proto \
                             ./hdmc/proto/localization/pose.proto \
                             ./hdmc/proto/localization/localization_status.proto \
                             ./hdmc/proto/localization/localization.proto \
                             ./hdmc/proto/map/map_id.proto \
                             ./hdmc/proto/map/map_geometry.proto \
                             ./hdmc/proto/map/topo_graph.proto \
                             ./hdmc/proto/map/map_clear_area.proto \
                             ./hdmc/proto/map/map_crosswalk.proto \
                             ./hdmc/proto/map/map_junction.proto \
                             ./hdmc/proto/map/map_lane.proto \
                             ./hdmc/proto/map/map_overlap.proto \
                             ./hdmc/proto/map/map_parking_space.proto \
                             ./hdmc/proto/map/map_pnc_junction.proto \
                             ./hdmc/proto/map/map_road.proto \
                             ./hdmc/proto/map/map_signal.proto \
                             ./hdmc/proto/map/map_speed_bump.proto \
                             ./hdmc/proto/map/map_speed_control.proto \
                             ./hdmc/proto/map/map_stop_sign.proto \
                             ./hdmc/proto/map/map_yield_sign.proto \
                             ./hdmc/proto/map/map.proto 

cd $EXEC_PATH
